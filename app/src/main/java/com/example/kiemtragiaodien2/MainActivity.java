package com.example.kiemtragiaodien2;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private EditText etHoTen,etNgaySinh,etPhone,etMail;
    private ImageView imageAvatar;
    private RadioButton rbNam,rbNu,rbKhac;
    private RadioGroup radioGroup;
    private CheckBox cbDocSach,cbAll,cbGame,cbSport;
    private Switch aSwitch;
    private ProgressBar progressBar;
    private Button btnDangKi,btnThoat;
    private int current = 0;
    private  boolean ischeck =false;
    private boolean isCheckBox=false;
    private boolean ischeckTextHoten = false;
    private boolean ischeckTextNgaySinh = false;
    private boolean ischeckTextPhone = false;
    private boolean ischeckTextMail = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();

        //Xử lí button Đăng ký
        btnDangKi.setOnClickListener(v -> {
            String mHoten = etHoTen.getText().toString();
            if(mHoten.isEmpty()) {
                Toast toast = Toast.makeText(MainActivity.this,"Họ tên không được bỏ trống",Toast.LENGTH_LONG);
                toast.setGravity(Gravity.BOTTOM|Gravity.CENTER_HORIZONTAL,0,-90);
                toast.show();
            }else {
                Toast toast = Toast.makeText(MainActivity.this,"Chào "+mHoten+"!\n Bạn đã đăng ký thành công",Toast.LENGTH_LONG);
                toast.setGravity(Gravity.BOTTOM|Gravity.CENTER_HORIZONTAL,0,-90);
                toast.show();
            }
        });

        btnThoat.setOnClickListener(v -> {
            Intent startMain = new Intent(Intent.ACTION_MAIN);
            startMain.addCategory(Intent.CATEGORY_HOME);
            startActivity(startMain);
            finish();
        });

        etHoTen.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!s.toString().isEmpty() && !ischeckTextHoten){
                    ischeckTextHoten = true;
                    current = progressBar.getProgress();
                    progressBar.setProgress(current+10);
                }else if(s.toString().isEmpty()) {
                    ischeckTextHoten = false;
                    current = progressBar.getProgress();
                    progressBar.setProgress(current-10);
                }
            }
            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        etNgaySinh.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!s.toString().isEmpty() && !ischeckTextNgaySinh){
                    ischeckTextNgaySinh = true;
                    current = progressBar.getProgress();
                    progressBar.setProgress(current+5);
                }else if(s.toString().isEmpty()) {
                    ischeckTextNgaySinh = false;
                    current = progressBar.getProgress();
                    progressBar.setProgress(current-5);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        etPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!s.toString().isEmpty() && !ischeckTextPhone){
                    ischeckTextPhone = true;
                    current = progressBar.getProgress();
                    progressBar.setProgress(current+5);
                }else if(s.toString().isEmpty()) {
                    ischeckTextPhone = false;
                    current = progressBar.getProgress();
                    progressBar.setProgress(current-5);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        etMail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!s.toString().isEmpty() && !ischeckTextMail){
                    ischeckTextMail = true;
                    current = progressBar.getProgress();
                    progressBar.setProgress(current+5);
                }else if(s.toString().isEmpty()) {
                    ischeckTextMail = false;
                    current = progressBar.getProgress();// oke chưa bạn
                    progressBar.setProgress(current-5);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }


    private void init() {
        etHoTen = findViewById(R.id.etHovaTen);
        etNgaySinh = findViewById(R.id.etNgaySinh);
        etPhone = findViewById(R.id.etPhoneNumber);
        etMail = findViewById(R.id.etEmail);
        radioGroup= findViewById(R.id.radGroup);
        imageAvatar = findViewById(R.id.imageView);
        imageAvatar.setImageResource(R.drawable.avatar);
        rbNam = findViewById(R.id.rabNam);
        rbNu = findViewById(R.id.rabNu);
        rbKhac = findViewById(R.id.rabKhac);
        cbDocSach = findViewById(R.id.cbDocSach);
        cbAll = findViewById(R.id.cbAll);
        cbGame = findViewById(R.id.cbChoiGame);
        cbSport = findViewById(R.id.cbSport);
        aSwitch = findViewById(R.id.switchToiDongy);
        progressBar = findViewById(R.id.progressBarTienTrinh);
        btnDangKi = findViewById(R.id.btnDangKi);
        btnThoat = findViewById(R.id.btnThoat);

    }




    public void CheckClick(View v){
        if(ischeck == false){
            current = progressBar.getProgress();
            progressBar.setProgress(current+25);
            ischeck = true;
        }
    }
    public void checkBox(View v){
        if(cbAll.isChecked()){
            cbSport.isEnabled();
            cbGame.setClickable(true);
            cbDocSach.setClickable(true);
        }
        if(isCheckBox == false){
            current = progressBar.getProgress();
            progressBar.setProgress(current+25);
            isCheckBox = true;
        }
    }

    public void checkBoxALL(View v){
        if(cbAll.isChecked()){
            cbDocSach.setChecked(false);
            cbSport.setChecked(false);
            cbGame.setChecked(false);
            cbDocSach.setClickable(false);
            cbSport.setClickable(false);
            cbGame.setClickable(false);
        }else{
            cbDocSach.setClickable(true);
            cbSport.setClickable(true);
            cbGame.setClickable(true);
        }
        if(isCheckBox == false){
            current = progressBar.getProgress();
            progressBar.setProgress(current+25);
            isCheckBox = true;
        }
    }

    public void checkSwif(View v){
        if(aSwitch.isChecked()) {
            current = progressBar.getProgress();
            progressBar.setProgress(current+25);
        }else {
            current = progressBar.getProgress();
            progressBar.setProgress(current-25);
        }
    }


}